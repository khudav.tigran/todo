import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/home/myComponent'
import Signin from '../components/Register/Signin'
import Forgot from '../components/Register/Login/Forgot'
import ForgotPassword from '../components/Register/Login/ForgotPassword'
import NotFound from '../components/NotFound/NotFound'
import userPage from '../components/Profile/userPage'
import Movies from '../components/Movies/MoviesComp'
import store from '../store/index'

const axios = require("axios").create({ baseURL: "http://localhost:5001/" });
Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Home,
            meta: { requiresAuth: true }
        },
        {
            name: "Login",
            path: '/signin',
            component: Signin,
            meta: { requiresAuth: true },
        },
        {
            name: 'ResetPass',
            path: '/signin/reset/',
            component: Forgot,
            meta: { requiresAuth: true }
        },
        {
            name: "Forgot",
            path: "/signin/reset/:resetId",
            component: ForgotPassword,
            meta: { requiresAuth: true }
        },
        {
            name: "Profile",
            path: '/profile',
            component: userPage,
            meta: { requiresAuth: true }
        },
        {
            name: 'Movies',
            path: "/movies",
            component: Movies
        },
        {
            name: '404',
            path: '/404',
            alias: '*',
            component: NotFound
        },
    ]
})


router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        const token = document.cookie.split("arev=")[1]
        if (to.name !== "Profile") {
            if (token) {
                axios.get("profile", {
                    headers: { Authorization: 'Bearer ' + token }
                }).then(() => next({ name: "Profile" })).catch(() => { next() })
            } else next()
        } else {
            if (token) {
                axios.get("profile", {
                    headers: { Authorization: 'Bearer ' + token }
                }).then((result) => {
                    store.commit("userData", result.data)
                    next()
                }).catch(() => { next({ name: "Login" }) })
            } else next({ name: "Login" })
        }
    } else next()
})

export default router