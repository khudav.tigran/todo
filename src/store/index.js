import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        axios: require("axios").create({ baseURL: "http://localhost:5001/", }),
        showerr: false,
        err: "",
        reg: /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/,
        isActive: true,
        profNotify: false,
        profSettings: false,
        ProfData: Object
    },
    mutations: {
        Erralert(state, errtext) {
            state.err = errtext;
            state.showerr = true;
            setTimeout(() => {
                state.showerr = false;
            }, 4500);
        },
        userData(state, data) {
            state.ProfData = data
        }
    },
    getters: {
        ErrArray: state => [state.showerr, state.err],
        axios: state => state.axios,
        reg: state => state.reg,
        isActive: state => state.isActive
    },
})